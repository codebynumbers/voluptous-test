import unittest
import app
import simplejson as json

class AppTestCase(unittest.TestCase):

    def setUp(self):
        """ Initialize test """
        app.app.config['TESTING'] = True
        self.app = app.app.test_client()
     
    def test_post_name_too_short(self):
        """ Test name length """
        resp = self.post({
        	    "name": "x"
            })
        self.assertTrue('errors' in resp)
        self.assertTrue('name' in resp['errors'])    
        
    def test_post_bid_too_low(self):
        """ Test bid is high enough """
        resp = self.post({
        	    "name": "abc",
                "default_bid": 0.02
            })
        self.assertTrue('errors' in resp)
        self.assertTrue('default_bid' in resp['errors']) 
        self.assertTrue('value must be at least' 
            in resp['errors']['default_bid'][0])
        
    def test_post_invalid_status(self):
        """ Test status is member of list """
        resp = self.post({
        	    "name": "abc",
                "status": 'Blah'
            })
        self.assertTrue('errors' in resp)
        self.assertTrue('status' in resp['errors'])
        self.assertTrue("expected 'Active', 'Paused' or 'Archived'"
            in resp['errors']['status'][0])                 

    def test_post_budget_wrong_type(self):
        """ Check type of budget """
        resp = self.post({
        	    "name": "abc",
                "default_bid": 0.10,
                "total_budget": "10.00"
            })
        self.assertTrue('errors' in resp)
        self.assertTrue(
            resp['errors'].get('total_budget')[0] == 'expected float')
        
    def test_post_ok(self):
        """ Check the happy path for POST """
        resp = self.post({
        	    "name": "abc",
                "default_bid": 0.20,
                "total_budget": 10.00,
                "status": "Active"
            })
        self.assertFalse('errors' in resp)
        self.assertTrue(resp.get('message') == "Valid") 
        
    def test_put_ok(self):
        """ Check the happy path for PUT """
        resp = self.put({
                "default_bid": 0.15
            })
        self.assertFalse('errors' in resp)
        self.assertTrue(resp.get('message') == "Valid") 
        
    def post(self, blob):
        """ POST Helper """
        return json.loads(self.app.post('/',
            headers={"Content-type": "application/json"},
            data=json.dumps(blob)).data)

    def put(self, blob):
        """ PUT helper """
        return json.loads(self.app.put('/',
            headers={"Content-type": "application/json"},
            data=json.dumps(blob)).data) 
    
if __name__ == '__main__':
    unittest.main()