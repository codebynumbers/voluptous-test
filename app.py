from flask import Flask, request, jsonify
from voluptuous import MultipleInvalid
from utility import format_errors, populate_obj
from schema_factory import SchemaFactory

app = Flask(__name__)
app.debug = True

class Campaign(object):
    """ Test object """
    
    name = "Test"
    default_bid = None
    total_budget = None
    status = None
    # This must NEVER change, must not exist in schema, 
    # if sent throws extra keys not allowed
    minimum_bid = 0.05
    
    def to_dict(self):
        """ Return object properties """
        return {
                "name": self.name,
                "default_bid": self.default_bid,
                "total_budget": self.total_budget,
                "status": self.status,
                "minimum_bid": self.minimum_bid
                }


@app.route('/', methods=["PUT","POST"])
def index():
    """ Default route for testing object update """

    # Dummy object for testing populate_obj
    camp = Campaign()    
        
    # Pick the right validation for the job
    schema = SchemaFactory.make_schema(request.method, camp.minimum_bid)

    # Validate schema
    try:
        validated = schema(request.json)
    except MultipleInvalid as exc:
        return jsonify(errors=format_errors(exc))
    else:
        populate_obj(schema, validated, camp)  

    return jsonify(message="Valid", object=camp.to_dict())


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)