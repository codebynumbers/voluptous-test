from voluptuous import Schema, Required, All, Any, Length, Range


class SchemaFactory(object):
    """ Factory for maing schemas """
    
    @classmethod
    def make_schema(cls, method, minimum_bid):
        """ Return appropriate schema for request """

        # Common validators
        name = All(unicode, Length(min=3))
        default_bid = All(float, Range(min=minimum_bid, max=2.0))
        total_budget = All(float, Range(min=1.00))
        status = Any('Active', 'Paused', 'Archived', 
            msg="expected 'Active', 'Paused' or 'Archived'")
        
        if method == 'POST':
            return Schema({
                           Required('name'): name,
                           Required('default_bid', 
                            default=minimum_bid): default_bid,
                           'total_budget': total_budget,
                           Required('status',  default='Active'): status
                           })
        elif method == 'PUT':
            return Schema({
                          'name': name,
                          'default_bid': default_bid,
                          'total_budget': total_budget,
                          'status': status  
                        })
        else:
            raise Exception("Invalid request method")
