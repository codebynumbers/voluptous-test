""" Utility methods """

def format_errors(exc):
    """ Format schema errors to our liking """
    result = {}
    for err in exc.errors:
        if not result.get(str(err.path[0])):
            result[str(err.path[0])] = []
        result[str(err.path[0])].append(err.error_message)
    return result


def populate_obj(schema_def, data, obj):
    """ Update object with data """
    for k in schema_def.schema:
        key = str(k)
        if hasattr(obj, key) and key in data:
            setattr(obj, key, data[key]) 